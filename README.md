
![소방관의 오늘](src/assets/imgs/logo.png)

# 소방관의 오늘

![ionic](https://img.shields.io/badge/Ionic-3.9.9-%23FFB789.svg)
![Version](https://img.shields.io/badge/Version-v0.0.1-%23FFEDAF.svg)

소방공무원 뇌기능 회복 매뉴얼 제공을 위한 모바일 어플리케이션 및 건강관리 시스템.

책자로 제작하는 소방공무원 뇌기능 회복 매뉴얼 제공하여 언제 어디서나 시간과 장소의 제한에 상관없이 원하는 정보를 쉽게 찾을 수 있으며 장소와 시간 제약에 구애받지 않고 메뉴얼의 정보를 열람할 수 있는 시스템.

## 목차

1. [개발환경](#개발환경)
2. [Getting Started](#getting-started)
3. [Device Started](#device-started)
4. [Android Build](#android-build)
5. [Project structure](#project-structure)
6. [Error](#error)
7. [License](#license)

---


## 개발환경

```
Ionic: 

   Ionic CLI          : 6.11.8
   Ionic Framework    : ionic-angular 3.9.9
   @ionic/app-scripts : 3.2.4

Cordova:

   Cordova CLI       : 9.0.0 (cordova-lib@9.0.1)
   Cordova Platforms : android 8.1.0
   Cordova Plugins   : cordova-plugin-ionic-keyboard 2.2.0, cordova-plugin-ionic-webview 4.2.1, (and 4 other plugins)

Utility:

   cordova-res (update available: 0.15.1) : 0.8.1
   native-run (update available: 1.1.0)   : 0.3.0

System:

   Android SDK Tools : 25.2.3
   ios-deploy        : 1.10.0
   ios-sim           : ios-sim/9.0.0 darwin-x64 node-v12.16.1
   NodeJS            : v12.16.1
   npm               : 6.14.4
```

---

## Getting Started

- Download the installer for [Node.js](https://nodejs.org/) 6 or greater.
- Install the ionic CLI & cordova globally: ```npm install -g ionic cordova```

```bash
$ npm i
$ ionic serve

Run `ionic serve` for a dev server. Navigate to `http://localhost:8100/`. 
The app will automatically reload if you change any of the source files.
```

### Device Started

Android
```
$ ionic cordova run android --device --livereload -c --address=0.0.0.0
```

## Android Build

### 1. Splash & icon 설정
~~~
$ ionic cordova resources
~~~

### 2. 빌드
~~~
$ ionic cordova build android --prod --release
~~~

### 3. 키스토어가 없을 경우 아래 명령어로 생성
~~~
keytool -genkey -v -keystore nfakiosk.jks -alias ikoob -keyalg RSA -keysize 2048 -validity 10000
~~~

### 4. 서명되지 않은 APK에 서명하기
~~~
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore nfakiosk.jks ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk ikoob
~~~

### 5. apk 파일 추출
~~~
/Users/eunhyeko/Library/Android/sdk/build-tools/29.0.2/zipalign -v 4 ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk nfakiosk.apk
~~~

---
## Project structure
```
.
├── app                             #
│   ├── app.component.ts            #
│   ├── app.html                    #
│   ├── app.module.ts               # 사용중인 모듈 및 페이지 관리
│   ├── app.scss                    # Golbal Style
│   └── main.ts                     #
├── assets                          #
│   ├── icon                        #
│   ├── imgs                        # 앱 사용 이미지
│   └── jsons                       # 키오스크 설문 Json file
│       ├── subject1.json           # 설문 Subject 1
│       ├── subject2.json           # 설문 Subject 2
│       ├── subject3.json           # 설문 Subject 3
│       ├── subject4.json           # 설문 Subject 4
│       ├── subject5.json           # 설문 Subject 5
│       ├── subject6.json           # 설문 Subject 6
│       ├── subject7.json           # 설문 Subject 7
│       ├── subject8.json           # 설문 Subject 8
│       └── subject_sample.json     # 모든 종류의 설문 기본 Json 모음( 모바일 앱 설문 포함 )
├── components                      # 
│   ├── progress-bar                # 설문 진행과정 Progress bar
│   └── surveys                     # 설문 모음
│       ├── survey-check1           # 다중선택 설문
│       ├── survey-date1            # 날짜/시간 설문
│       ├── survey-input1           # 글자입력 설문 1 (기본형)
│       ├── survey-input2           # 글자입력 설문 2 (입력 활성/비활성)
│       ├── survey-input3           # 글자입력 설문 3 (input1 과 스타일이 다름)
│       ├── survey-input4           # 글자입력 설문 4 (그룹)
│       ├── survey-more1            # 추가 설문1 (선택한 문항에 추가설문이 있을 경우 진행)
│       ├── survey-more2            # 추가 설문2 (버튼을 눌러 설문그룹을 직접 추가하여 진행)
│       ├── survey-more3            # 추가 설문3 (활성/비활성에 따라 추가 진행)
│       ├── survey-radio1           # 단일선택 설문1 (기본형)
│       ├── survey-radio2           # 단일선택 설문2 (활성/비활성에 따라 진행 가능)
│       ├── survey-range1           # 범위형 설문1 (기본형)
│       └── survey-textarea1        # 장문 설문1 (기본형)
├── directives                      #
│   └── for-id                      # lable for to id Directive
├── pages                           #
│   ├── home                        # 키오스크 설문 메인 화면
│   ├── success                     # 키오스크 설문 종료 화면
│   └── survey                      # 키오스크 설문 진행 화면
├── pipes                           #
│   └── safe-html                   # JSON text의 스타일 적용 Pipe
├── providers                       #
│   ├── answer                      # 설문 서비스
│   └── api                         # 서버 연동 서비스
└── theme                           # App Theme

```


---
## Error

### Splash & Icon 설정시
```
$ ionic cordova resources
$ npm install -g cordova-res

npm WARN checkPermissions Missing write access to /usr/local/lib/node_modules
npm ERR! path /usr/local/lib/node_modules
npm ERR! code EACCES
npm ERR! errno -13
.
.
.
```
아래 실행

```
$ sudo chown -R $USER /usr/local/lib/node_modules
```

---

## License

Copyright © 2020 iKooB Inc. All Rights Reserved.
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';

import { DirectivesModule } from '../directives/directives.module';
import { ComponentsModule } from '../components/components.module';
import { HttpClientModule } from '@angular/common/http';

import { HomePage } from '../pages/home/home';
import { SuccessPage } from '../pages/success/success';
import { SurveyPage } from '../pages/survey/survey';
import { PipesModule } from '../pipes/pipes.module';
import { ApiProvider } from '../providers/api/api';
import { AnswerProvider } from '../providers/answer/answer';

const pages = [
  HomePage,
  SurveyPage,
  SuccessPage,
]

@NgModule({
  declarations: [
    MyApp,
    ...pages
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    DirectivesModule,
    HttpClientModule,
    PipesModule,
    IonicModule.forRoot(MyApp, {
      mode: 'ios',
      backButtonText: ''
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ...pages
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiProvider,
    AnswerProvider
  ]
})
export class AppModule { }

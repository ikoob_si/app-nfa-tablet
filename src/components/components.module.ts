import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../pipes/pipes.module';
import { BrowserModule } from '@angular/platform-browser';
import { DirectivesModule } from '../directives/directives.module';

import { ProgressBarComponent } from './progress-bar/progress-bar';
import { SurveyDate1Component } from './surveys/survey-date1/survey-date1';
import { SurveyMore1Component } from './surveys/survey-more1/survey-more1';
import { SurveyInput1Component } from './surveys/survey-input1/survey-input1';
import { SurveyRadio1Component } from './surveys/survey-radio1/survey-radio1';
import { SurveyRadio2Component } from './surveys/survey-radio2/survey-radio2';
import { SurveyCheck1Component } from './surveys/survey-check1/survey-check1';
import { SurveyInput2Component } from './surveys/survey-input2/survey-input2';
import { SurveyInput3Component } from './surveys/survey-input3/survey-input3';
import { SurveyInput4Component } from './surveys/survey-input4/survey-input4';
import { SurveyRange1Component } from './surveys/survey-range1/survey-range1';
import { SurveyTextarea1Component } from './surveys/survey-textarea1/survey-textarea1';
import { SurveyMore2Component } from './surveys/survey-more2/survey-more2';
import { SurveyMore3Component } from './surveys/survey-more3/survey-more3';

const components = [
	ProgressBarComponent,
	SurveyDate1Component,
	SurveyInput1Component,
	SurveyInput2Component,
	SurveyRadio1Component,
	SurveyRadio2Component,
	SurveyCheck1Component,
	SurveyMore1Component,
	SurveyMore2Component,
	SurveyInput3Component,
	SurveyInput4Component,
	SurveyRange1Component,
	SurveyTextarea1Component,
	SurveyMore3Component,
]

@NgModule({
	declarations: [...components,],
	imports: [
		IonicModule,
		BrowserModule,
		DirectivesModule,
		PipesModule,
	],
	exports: [...components,]
})
export class ComponentsModule { }

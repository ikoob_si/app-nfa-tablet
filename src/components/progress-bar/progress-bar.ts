import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'progress-bar',
  templateUrl: 'progress-bar.html'
})
export class ProgressBarComponent implements OnInit {
  @Input('lists') lists: number;
  @Input('target') target: number = 0;

  boxs: any[] = [];

  constructor() { }

  ngOnInit() {
    this.boxs = Array(this.lists).fill(this.lists).map((x, i) => i);
  }

}

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyCheck1Component component.
 * * 다중선택(체크박스) 설문 기본형
 * * 설문에 답이 하나라도 들어가 있어야 한다.
 */
@Component({
  selector: 'survey-check1',
  templateUrl: 'survey-check1.html'
})
export class SurveyCheck1Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
  }

  // * 유효성 검사
  public validCheck() {
    return Observable.create(observer => {
      this.survey.value = '';
      this.survey.etc = '';
      this.survey.valid = false;
      let all = true;
      this.survey.list.forEach(item => {
        if (item.checked) {
          if (this.survey.value.length) this.survey.value += ',';
          if (item.role === 'etc') {
            if (item.value && item.value.length) {
              if (this.survey.etc.length) this.survey.etc += '|';
              this.survey.etc += item.value;
            } else {
              all = false;
            }
          }
          this.survey.value += item.id;
        }
      });
      if (this.survey.value && all) this.survey.valid = true;
      observer.next();
    });
  }

  // * 체크박스 변화 감지
  public setValue($event, item) {
    if (!$event.value && item.role === 'etc')  item.value = ''
    this.validCheck().subscribe(() => {
      let answers = {};
      answers[this.survey.id] = this.survey.value;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

  // * 기타입력 변화 감지
  public setEtc() {
    this.validCheck().subscribe(() => {
      let answers = {};
      answers[`${this.survey.id}_t`] = this.survey.etc;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

}

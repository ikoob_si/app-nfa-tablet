import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { AnswerProvider } from '../../../providers/answer/answer';
import { Observable } from 'rxjs';

/**
 * Generated class for the SurveyDate1Component component.
 * * 날짜 설문 기본형
 */

@Component({
  selector: 'survey-date1',
  templateUrl: 'survey-date1.html'
})
export class SurveyDate1Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  public date: string;
  public format = {
    year: false,
    month: false,
    date: false,
    hour12: false,
    hour24: false,
    minute: false,
  }

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
    switch (this.survey.formatType) {
      case 'date':
        this.format.year = this.survey.format.indexOf('YYYY') > -1;
        this.format.month = this.survey.format.indexOf('MM') > -1;
        this.format.date = this.survey.format.indexOf('DD') > -1;
        break;
      case 'time12':
        this.format.hour12 = this.survey.format.indexOf('HH') > -1;
        this.format.minute = this.survey.format.indexOf('mm') > -1;
        break;
      case 'time24':
        this.format.hour24 = this.survey.format.indexOf('H') > -1;
        this.format.minute = this.survey.format.indexOf('mm') > -1;
        break;
    }
  }

  // * 유효성 검사
  public validCheck() {
    return Observable.create(observer => {
      this.survey.valid = false;
      if (!this.survey.checked && this.survey.value === this.survey.optionalText) {
        this.survey.valid = false;
        let answers = {};
        answers[this.survey.id] = undefined;
        this.awp.setAnswers(answers);
      } else {
        if (this.survey.value) this.survey.valid = true;
      }
      observer.next()
    });
  }

  // * 입력 변화 감지
  public setValue() {
    this.validCheck().subscribe(() => {
      let answers = {};
      answers[this.survey.id] = this.survey.value;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

  // * 옵션 변화 감지
  public setOptional() {
    if (this.survey.checked) this.survey.value = this.survey.optionalText;
    let answers = {};
    answers[this.survey.id] = this.survey.value;
    this.awp.setAnswers(answers);
    this.validCheck().subscribe(() => {
      this.changeFn.emit();
    });
  }

}

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyInput1Component component.
 * * 주관식 단답 설문 기본형
 * * 설문의 답을 합쳐서 저장합니다. 예) 치료의사|홍길동
 */

@Component({
  selector: 'survey-input1',
  templateUrl: 'survey-input1.html'
})
export class SurveyInput1Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
  }

  // * 유효성 검사
  public validCheck() {
    return Observable.create(observer => {
      this.survey.value = '';
      this.survey.valid = false;
      let all = true;
      this.survey.list.forEach(answer => {
        if (answer.value) {
          if (this.survey.value.length) this.survey.value += (this.survey.division || '|');
          this.survey.value += answer.value;
        } else {
          all = false;
        }
      });
      if (this.survey.value && all) this.survey.valid = true;
      observer.next()
    });
  }

  // * 입력 변화 감지
  public setValue() {
    this.validCheck().subscribe(() => {
      let answers = {};
      answers[this.survey.id] = this.survey.value;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

}

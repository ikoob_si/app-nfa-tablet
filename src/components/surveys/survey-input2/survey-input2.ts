import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyInput1Component component.
 * * input 설문 활성화형
 * * 활성 여부에 따라 설문 활성화
 * * 설문의 답을 각각 저장합니다.
 *
 */
@Component({
  selector: 'survey-input2',
  templateUrl: 'survey-input2.html'
})
export class SurveyInput2Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
    if (this.survey.value === undefined) this.survey.value = '0';
  }

  // * 유효성 검사
  public validCheck() {
    return Observable.create(observer => {
      this.survey.valid = false;
      let answers = {};
      let all = true;
      this.survey.list.forEach(item => {
        if (!item.value) all = false;
        if (this.survey.value === '0') item.value = '';
        answers[item.id] = item.value;
      });
      this.survey.valid = this.survey.value === '0' ? true : all;
      observer.next(answers);
    })
  }

  // * 입력 변화 감지
  public setValue() {
    this.validCheck().subscribe((answers) => {
      if (this.survey.saveMe) answers[this.survey.id] = this.survey.value;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

}

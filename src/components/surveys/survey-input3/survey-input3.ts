import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyInput3Component component.
 * * survey value 저장하지 않음.
 * * 주관식 단답 설문 array형
 * * 설문의 답을 각각 저장합니다.
 */

@Component({
  selector: 'survey-input3',
  templateUrl: 'survey-input3.html'
})
export class SurveyInput3Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
  }

  // * 유효성 검사
  public invalidCheck() {
    this.survey.valid = false;
    return Observable.create(observer => {
      let answers = {};
      let all = true;
      this.survey.list.forEach(item => {
        if (!item.value) all = false;
        answers[item.id] = item.value;
      });
      this.survey.valid = all;
      observer.next(answers);
    });
  }

  // * 입력 변화 감지
  public setValue() {
    this.invalidCheck().subscribe((answers) => {
      this.awp.setAnswers(answers);
      this.changeFn.emit({ survey: this.survey });
      this.changeFn.emit();
    });
  }

}

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

@Component({
  selector: 'survey-input4',
  templateUrl: 'survey-input4.html'
})
export class SurveyInput4Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
  }

  // * 유효성 검사
  public invalidCheck() {
    return Observable.create(observer => {
      let all = true;
      let answers = {};
      this.survey.valid = false;
      this.survey.list.forEach(item => {
        item.list.map(answer => {
          if (!answer.value) all = false;
          answers[answer.id] = answer.value;
        });
      });
      this.survey.valid = all;
      observer.next(answers);
    });
  }

  // * 입력 변화 감지
  public setValue() {
    this.invalidCheck().subscribe((answers) => {
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

}

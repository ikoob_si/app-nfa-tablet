import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyInput1Component component.
 * * 선택한 답변에 따라 추가되는 설문 기본형
 */

@Component({
  selector: 'survey-more1',
  templateUrl: 'survey-more1.html'
})
export class SurveyMore1Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
    if (this.survey.selected === undefined) this.survey.selected = {};
  }

  // * 유효성 검사
  public invalidCheck() {
    return Observable.create(observer => {
      let answers = {};
      this.survey.valid = true;
      if (!this.survey.selected.id) {
        this.survey.valid = false;
      } else {
        this.survey.list.forEach((item) => {
          if (item.id === this.survey.selected.id) { // 활성 
            if (!item.surveys) return;
            let invalid = item.surveys.find(survey => {
              if (survey.type === 'more2' && !survey.lists.length) return;
              return !survey.valid;
            });
            if (invalid) this.survey.valid = false;
          } else { // 비활성
            if (item.surveys) {
              this.awp.removeAnswers(item.surveys);
            }
          }
        });
      }
      observer.next(answers);
    });
  }

  // * 설문 선택 변경
  public setActive() {
    this.invalidCheck().subscribe((answers) => {
      if (this.survey.saveMe) answers[this.survey.id] = this.survey.selected.id;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

  // * 문항 변경 감지
  public setValue() {
    this.invalidCheck().subscribe((answers) => {
      if (this.survey.saveMe) answers[this.survey.id] = this.survey.selected.id;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

}

import { Component, Input, OnInit, Output, EventEmitter, } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyMore2Component component.
 * * + 버튼 클릭시 추가되는 설문 형태
 * * - 버튼 클릭시 해당 설문 삭제
 * * more2 형태안에 more1 설문은 불가능합니다.
 * * more2 형태안에 more2 설문은 불가능합니다.
 * * more2 형태안에 more3 설문은 불가능합니다.
 */

@Component({
  selector: 'survey-more2',
  templateUrl: 'survey-more2.html'
})
export class SurveyMore2Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider,) { }

  ngOnInit() {
    this.survey.valid = true;
  }

  // * 설문 추가하기
  public addGroup() {
    let group = JSON.parse(JSON.stringify(this.survey.group));
    group.forEach((item: any) => {
      item.id = `${this.survey.id}${this.survey.lists.length + 1}_${item.id}`;
      switch (item.type) {
        case 'input2':
          item.list.map(a => {
            a.id = `${this.survey.id}${this.survey.lists.length + 1}_${a.id}`;
          })
          break;
        case 'input3':
          item.list.map(a => {
            a.id = `${this.survey.id}${this.survey.lists.length + 1}_${a.id}`;
          })
          break;
        case 'input4':
          item.list.forEach(a => {
            a.list.map(i => {
              i.id = `${this.survey.id}${this.survey.lists.length + 1}_${i.id}`;
            })
          })
          break;
      }
    });
    this.survey.lists.push(Object.assign([], group));
    this.invalidCheck().subscribe(() => {
      this.changeFn.emit();
    });
  }

  // * 설문 삭제하기
  public removeGroup(index: number) {
    this.awp.removeAnswers(this.survey.lists[index]);
    this.survey.lists.splice(index, 1);
    this.invalidCheck().subscribe(() => {
      this.changeFn.emit();
    });
  }

  // * 유효성 검사
  public invalidCheck() {
    return Observable.create(observer => {
      this.survey.valid = true;
      for (let list of this.survey.lists) {
        let result = list.find(item => {
          return !item.valid
        });
        if (result) this.survey.valid = false;
      }
      observer.next();
    });
  }

  // 설문 변경시 호출
  public setValue() {
    this.invalidCheck().subscribe(() => {
      this.changeFn.emit();
    });
  }

}

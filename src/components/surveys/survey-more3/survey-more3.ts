import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyMore3Component component.
 * * 활성 여부에 따라 추가 설문을 진행할 수 있으며 group 형태입니다.
 */


@Component({
  selector: 'survey-more3',
  templateUrl: 'survey-more3.html'
})
export class SurveyMore3Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
    if (this.survey.value === undefined) this.survey.value = '0';
  }

  // * 유효성 검사
  public invalidCheck() {
    let all = true;
    return Observable.create(observer => {
      if (this.survey.value === '1') {
        let result = this.survey.list.find(item => {
          return !item.valid
        });
        if (result) all = false;
      }
      this.survey.valid = all;
      observer.next();
    });
  }

  // * 활성 변경 감지
  public setActive() {
    this.invalidCheck().subscribe(() => {
      let answers = {};
      if (this.survey.saveMe) answers[this.survey.id] = this.survey.value;
      if (this.survey.value === '0') {
        this.awp.removeAnswers(this.survey.list);
      }
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

  // * 입력 변경 감지
  public setValue() {
    this.invalidCheck().subscribe(() => {
      this.changeFn.emit();
    });
  }

}

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyRadio1Component component.
 * * 단일선택(라디오박스) 설문 기본형
 * * 설문에 답이 있어야 한다.
 */
@Component({
  selector: 'survey-radio1',
  templateUrl: 'survey-radio1.html'
})
export class SurveyRadio1Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
  }

  // * 유효성 검사
  public validCheck() {
    return Observable.create(observer => {
      this.survey.etc = '';
      this.survey.valid = this.survey.value ? true : false;
      this.survey.list.forEach(item => {
        if (item.id === this.survey.value) {
          if (item.role === 'etc') {
            if (!item.value) this.survey.valid = false;
            this.survey.etc = item.value;
          }
        } else {
          if (item.role === 'etc') item.value = '';
        }
      });
      observer.next();
    });
  }

  public setValue() {
    this.validCheck().subscribe(() => {
      let answers = {};
      answers[this.survey.id] = this.survey.value;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

  public setEtc() {
    this.validCheck().subscribe(() => {
      let answers = {};
      answers[`${this.survey.id}_t`] = this.survey.etc;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }


}

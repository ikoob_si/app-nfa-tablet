import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyRadio2Component component.
 * * 단일 선택(라디오) 설문 활성화형
 * * 활성 여부에 따라 설문 활성화 (활성 여부는 저장되지 않음.)
 * * 기타 항목은 작성할 수 없음.
 */
@Component({
  selector: 'survey-radio2',
  templateUrl: 'survey-radio2.html'
})
export class SurveyRadio2Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.active === undefined) this.survey.active = 0;
    if (this.survey.valid === undefined) this.survey.valid = true;
  }

  // * 유효성 검사
  public validCheck() {
    return Observable.create(observer => {
      if (this.survey.active) {
        this.survey.valid = this.survey.value ? true : false;
      } else {
        this.survey.value = '';
      }
      observer.next();
    });
  }

  // * 입력 변화 감지
  public setValue() {
    this.validCheck().subscribe(() => {
      let answers = {};
      answers[this.survey.id] = this.survey.value;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

}

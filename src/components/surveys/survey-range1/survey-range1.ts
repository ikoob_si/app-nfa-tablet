import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyRange1Component component.
 * * 단일 범위형 설문
 */
@Component({
  selector: 'survey-range1',
  templateUrl: 'survey-range1.html'
})
export class SurveyRange1Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    // * 해당 설문은 값을 0으로 해도 응답을 한 것으로 간주하므로 항상 유효함
    if (this.survey.valid === undefined) this.survey.valid = true;
    if (this.survey.value === undefined) this.survey.value = 0;
  }

  // * 입력 상태 변경
  public setValue() {
    let answers = {};
    answers[this.survey.id] = this.survey.value.toString();
    this.awp.setAnswers(answers);
    this.changeFn.emit();
  }
}

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../../providers/answer/answer';

/**
 * Generated class for the SurveyRadio2Component component.
 * * 주관식 설문유형 (장문)
 *
 */
@Component({
  selector: 'survey-textarea1',
  templateUrl: 'survey-textarea1.html'
})
export class SurveyTextarea1Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
  }

  // * 유효성 검사
  public validCheck() {
    return Observable.create(observer => {
      this.survey.valid = this.survey.value ? true : false;
      observer.next();
    });
  }

  // * 입력 상태 변경
  public setValue() {
    this.validCheck().subscribe(() => {
      let answers = {};
      answers[this.survey.id] = this.survey.value;
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

}

import { NgModule } from '@angular/core';
import { ForIdDirective } from './for-id/for-id';
@NgModule({
	declarations: [
		ForIdDirective
	],
	imports: [],
	exports: [
		ForIdDirective
	]
})
export class DirectivesModule {}

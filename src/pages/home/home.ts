import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { SurveyPage } from '../survey/survey';
import { ApiProvider } from '../../providers/api/api';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public code: string = '';

  constructor(
    private api: ApiProvider,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
  ) { }

  // * 코드 추가하기
  public addCode(code: number) {
    this.code += code.toString();
  }

  // * 코드 지우기
  public removeCode() {
    if (!this.code.length) return;
    // * 하나씩 지울 때 사용
    this.code = this.code.substring(0, this.code.length - 1);
    // * 전체 삭제 사용
    // this.code = '';
  }

  // * 개인 식별코드 확인
  public checkCode() {
    this.api.findCode(this.code).subscribe(
      (response: any) => {
        this.navCtrl.push(SurveyPage, { subject: 1, page: 0, patient: response });
      },
      () => {
        let alert = this.alertCtrl.create({
          title: '확인',
          subTitle: '등록되지 않은 코드입니다.<br>개인 식별 코드를 정확하게 입력해 주세요.',
          buttons: [{
            text: '확인',
            cssClass: 'confirm-btn'
          }]
        });
        alert.present();
      });
  }

}

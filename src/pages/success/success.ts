import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-success',
  templateUrl: 'success.html',
})
export class SuccessPage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() { }

  // 메인으로 돌아가기
  public goHome() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'back' });
  }

}

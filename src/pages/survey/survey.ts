import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, Platform, Navbar, Content } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SuccessPage } from '../success/success';
import { AnswerProvider } from '../../providers/answer/answer';

@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html',
})
export class SurveyPage implements OnDestroy {
  @ViewChild(Navbar) navBar: Navbar;
  @ViewChild(Content) content: Content;

  // * 설문 파일 리스트
  public subjectList: any = ['subject1', 'subject2', 'subject3', 'subject4', 'subject5', 'subject6', 'subject7', 'subject8'];

  public page: number = 0; // 선택된 설문의 스탭
  public subject: number = 0; // 선택된 설문 번호 
  public subjects: any = []; // 다운로드한 설문 목록
  public patient: any; // 설문자 정보
  public alert: any;

  public unregisterBackButtonAction: any;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    private platform: Platform,
    private awp: AnswerProvider,
  ) { }

  ngOnDestroy() {
    if (this.alert) this.alert.dismiss();
    if(this.unregisterBackButtonAction) this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    if (this.navParams.data) {
      this.getSubject(this.navParams.data.subject, this.navParams.data.page);
      this.patient = this.navParams.data.patient;
      console.log(this.navParams.data)
    }

    // * 안드로이드에서 백버튼 클릭 시
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.leaveSurvey();
    }, 2);

    // * 백버튼 클릭 시
    this.navBar.backButtonClick = () => {
      this.leaveSurvey();
    }
  }

  // * 설문을 벗어나고자 할 때
  public leaveSurvey() {
    if(this.alert){
      this.alert.dismiss();
      this.alert = null;
    } else {
      this.alert = this.alertCtrl.create({
        title: '확인',
        subTitle: '설문을 종료하시겠습니까?',
        buttons: [
          {
            text: '취소',
            role: 'cancel',
            cssClass: 'cancel-btn'
          },
          {
            text: '확인',
            cssClass: 'confirm-btn',
            handler: () => {
              this.awp.removeAllAnswers().subscribe(() => {
                this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'back' });
              });
            }
          }
        ]
      })
      return this.alert.present();
    }
  }

  // * 설문 불러오기
  public getSubject(subject: number, page: number) {
    this.page = page; // 선택된 스탭 저장
    this.subject = subject; // 선택된 설문 번호 저장
    if (this.subjects[this.subject]) return;  // 가져온 설문이 이미 있을 경우 호출하지 않음
    this.awp.getSubJect(subject).subscribe((resp: any) => {
      this.subjects[this.subject] = resp; // 설문 저장
    });
  }

  // * 이전 스탭으로 이동
  public prevPage() {
    if (this.page === 0 && this.subject === 1) {
      // * 첫 스탭일 경우
      this.leaveSurvey();
    } else {
      if (this.page === 0) {
        this.content.scrollToTop();
        this.getSubject(this.subject - 1, this.subjects[this.subject - 1].pages.length - 1);
      } else {
        this.content.scrollToTop();
        this.page--;
      }
    }
  }

  // * 다음 스탭으로 이동
  public nextPage() {
    let surveys = this.subjects[this.subject].pages[this.page].surveys;
    // * 유효성 검사 실패
    let invalid = surveys.find(survey => {
      return !survey.valid;
    });
    if (invalid) { // * 유효성 검사 실패
      let alert = this.alertCtrl.create({
        title: '확인',
        subTitle: '답변이 입력되지 않은 설문이 있습니다.<br>설문에 답변해주세요.',
        buttons: [{
          text: '확인',
          cssClass: 'confirm-btn'
        }]
      })
      return alert.present();

    } else {
      // * 현재 스탭이 현 설문의 마지막 스탭일 경우 다음 설문으로 이동
      if (this.page === this.subjects[this.subject].pages.length - 1) {
        if (this.subject === this.subjectList.length) {
          // * 마지막 설문일 경우 설문 저장 후 완료 페이지로 이동
          this.sendAnswers();
        } else {
          this.content.scrollToTop();
          this.getSubject(this.subject + 1, 0);
        }
      } else {
        this.content.scrollToTop();
        this.page++;
      }
    }
  }


  // * 설문 종료 / 답변 저장하기
  public sendAnswers() {
    let params: any = {
      surveyType: '4',
      answers: this.awp.answers
    }
    this.awp.postAnswers(this.patient, params).subscribe((response) => {
      this.navCtrl.setRoot(SuccessPage, {}, { animate: true, direction: 'forward' });
    })
  }

}

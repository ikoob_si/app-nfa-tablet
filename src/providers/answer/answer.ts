import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { ApiProvider } from '../api/api';


@Injectable()
export class AnswerProvider {

  public answers: any = {}

  constructor(public http: HttpClient, private api:ApiProvider) { }

  // * 설문 가져오기
  public getSubJect(subject: number): Observable<any> {
    return this.http.get(`assets/jsons/subject${subject}.json`)
      .pipe(catchError(this.handleError));
  }

  // * 설문 답변 가져오기
  public getAnswers(): Observable<any> {
    return Observable.create(observer => {
      observer.next(this.answers);
    });
  }

  // * 설문 변경에 따라 실시간 변경됨.
  public setAnswers(answers: any) {
    Object.keys(answers).forEach(key => {
      if (answers[key] && answers[key].length) {
        this.answers[key] = answers[key]
      } else {
        delete this.answers[key];
      }
    });
  }

  // * 저장된 설문 답변 초기화
  public removeAnswers(surveys) {
    surveys.forEach(survey => {
      switch (survey.type) {
        case 'input4':
          survey.list.forEach(item => {
            if (item.list) this.removeAnswers(survey.list);
          });
          break;
        case 'radio2':
          survey.active = 0;
          break;
        case 'more1':
          survey.list.forEach(item => {
            if (!item.surveys) return;
            this.removeAnswers(item.surveys);
          });
          break;
        case 'more2':
          if (survey.lists) {
            survey.lists.forEach(list => {
              if (list) this.removeAnswers(list);
            });
            survey.lists = [];
          }
          break;
      }
      delete this.answers[survey.id];
      delete this.answers[`${survey.id}_t`];
      delete survey.value;
      delete survey.checked;
      delete survey.selected;
      if (survey.list) this.removeAnswers(survey.list);
    });
  }

  // * 저장된 설문 저장하기
  public postAnswers(patient: any, params: any) {
    let endpoint = `/api/v1/patient/${patient.id}/result`;
    return this.api.post(endpoint, params);
  }

  // * 설문 모든 답변 삭제하기
  public removeAllAnswers(): Observable<any> {
    return Observable.create(observer => {
      this.answers = {};
      observer.next();
    });
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return Observable.throw(error);
  }

}

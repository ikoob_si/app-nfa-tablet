import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

@Injectable()
export class ApiProvider {

  BASE_URL: string = 'http://nfa-api.cade.education:8041';

  constructor(public http: HttpClient) { }

  /**
   * 소방관앱 개인 식별코드 조회
   * @param code 개인 식별코드
   */
  findCode(code: string) {
    let endpoint = '/api/v1/find/code';
    return this.post(endpoint, { code: code });
  }

  get(endpoint: string, body?: any): Observable<any> {
    return this.http.get(this.BASE_URL + endpoint, body)
      .pipe(catchError(this.handleError));
  }

  post(endpoint: string, body?: any): Observable<any> {
    return this.http.post(this.BASE_URL + endpoint, body)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${JSON.stringify(error)}`);
    }
    return Observable.throw('Something bad happened; please try again later.');
  }

}
